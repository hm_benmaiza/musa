from flask import Flask,request,jsonify,send_file
from PIL import Image


import tensorflow as tf

import io
import pandas as pd
import numpy as np
from numpy import asarray
import json
import re
from emot.emo_unicode import UNICODE_EMOJI,EMOTICONS_EMO

import nltk
import urllib.request
from io import StringIO
from werkzeug.utils import secure_filename
import json



import tensorflow_text as text

import tensorflow_hub as hub
from tensorflow.keras.layers import BatchNormalization,Dense,Input,LSTM,Embedding,Conv2D,MaxPooling2D,GlobalAveragePooling2D,Concatenate,Dropout,Bidirectional,GlobalMaxPooling2D
from tensorflow.keras.models import Sequential,Model,load_model

import time
from model import CreateModel

from preprocess_text import textpreprocessing

app=Flask(__name__)

UPLOAD_FOLDER='/statics/uploads'
app.secret_key="imed-mohamed"
app.config['UPLOAD_FOLDER']=UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH']= 16 *1024 *1024


#d = pd.DataFrame({"Text": ["SUNDAYS ARE FOR THE DEAD. #FearTheWalkingDead #FearBeginsHere @FearTWD"]})
#display(d.iloc[0]['Text'])

ALLOWED_EXTENSIONS =set(['png','jpg','jpeg','gif'])
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.',1)[1].lower() in ALLOWED_EXTENSIONS





#display(d.iloc[0]['Text'])


print("###########################################")

model=CreateModel()
print("The model was uploaded successfully")
print("###########################################")
url2="http://127.0.0.1:7860/"





@app.route('/', methods=['POST'])
def home():

    response_gr=json.loads(request.data)
    img_arr = np.asarray(response_gr["Image"])

    
    txt = io.StringIO(response_gr["Text"])
    
    df = pd.DataFrame(txt, columns=["text"])
    textpreprocessing(df)
    
    txt_arr=df["text"].to_numpy()
    txt_final=np.expand_dims(txt_arr,axis=0)
    

    img_arr=tf.keras.layers.Resizing(224,224)(img_arr)

    image_preprocessed= tf.keras.applications.inception_resnet_v2.preprocess_input(img_arr)
    image_finale=np.expand_dims(image_preprocessed, axis=0)
    print(image_finale.shape)

    predictions = model.predict((image_finale, txt_final))
    
    prediction_max= np.argmax(predictions)
    
    
    if prediction_max==0 :
        sent="negatif"
    elif prediction_max==1 :
        sent="neutre"
    else :
        sent="positif"
    
    #payload_fin = {"Text_fin": str(df.iloc[0]['text']), "Image_fin": image_preprocessed.tolist(), "score":prediction_max.tolist()}
    #payload_fin = json.dumps(payload_fin)

    #tx_fin = requests.post(url2, data=payload_fin)
    #print(tx_fin.status_code)


    response =app.response_class(
        response=json.dumps({"text_prepro": str(df.iloc[0]['text']), "sentiment":sent}),
        mimetype='application/json'
    )

    return response










if __name__=='__main__':
    app.run(debug=False)












