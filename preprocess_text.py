import tensorflow as tf
import pandas as pd
import numpy as np
import re
from emot.emo_unicode import UNICODE_EMOJI,EMOTICONS_EMO
from emot.emo_unicode import UNICODE_EMOJI,EMOTICONS_EMO
import time


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.',1)[1].lower() in ALLOWED_EXTENSIONS



def hashtag_split(word):
    if word.islower():
        return word
    else:
        new_word = ''
        list_words = []
        prev = ""
        for c in word:

            if c.isupper() and prev.islower():
                list_words.append(new_word)
                new_word = ""
            new_word += c
            prev = c
        if new_word != "": list_words.append(new_word)

        return " ".join(list_words).strip()


def hashtags(text):
    words = text.split(" ")
    new_words = []
    for w in words:
        if w != "":
            i = 0
            word = []
            while w[i] != "#" and i < len(w) - 1:
                word.append(w[i])
                i += 1

            if w[i] == "#":
                x = hashtag_split(w[i + 1:])
                new_words.append("".join(word) + "#" + x)
            else:
                word.append(w[i])
                new_words.append("".join(word))
    return " ".join(new_words)

def decontracted(phrase):
    # specific
    phrase = re.sub(r"won\'t", "will not", phrase)
    phrase = re.sub(r"can\'t", "can not", phrase)
    phrase = re.sub(r"shouldn\'t", "should not", phrase)

    # general
    phrase = re.sub(r"n\'t", " not", phrase)
    phrase = re.sub(r"\'re", " are", phrase)
    phrase = re.sub(r"\'s", " is", phrase)
    phrase = re.sub(r"\'d", " would", phrase)
    phrase = re.sub(r"\'ll", " will", phrase)
    phrase = re.sub(r"\'t", " not", phrase)
    phrase = re.sub(r"\'ve", " have", phrase)
    phrase = re.sub(r"\'m", " am", phrase)
    return phrase

def convertemoji(text):
  for emot in UNICODE_EMOJI:
    text=text.replace(emot,UNICODE_EMOJI[emot])
  return text

for k in EMOTICONS_EMO.keys():
  EMOTICONS_EMO[k]=EMOTICONS_EMO[k].split(",")[0]
def convertemoticons(text):
  for emot in EMOTICONS_EMO:
    if emot in text.split(" "):
      text=text.replace(emot,EMOTICONS_EMO[emot])
  return text


def textpreprocessing(data):
    
    # remove RT cc
    data["text"] = data['text'].str.replace("\s*RT\s+|\s+cc", "",regex=True)

    # urls
    data['text'] = data['text'].str.replace(r'http\S*', '',regex=True)

    #split hashtags
    data["text"] = data['text'].apply(lambda x: hashtags(x))

   
    # convert emoticons
    data['text'] = data['text'].apply(convertemoticons)

    # remove numbers and words containing numbers
    data["text"] = data['text'].str.replace("\w*\d+\w*", " ",regex=True)

    # convert emoji
    data['text'] = data['text'].apply(convertemoji)

    # lower case
    data['text'] = data['text'].str.lower()

    # decontract
    data['text'] = data['text'].apply(lambda x: decontracted(x))

    #remove duplicate space
    data["text"] = data['text'].str.replace("\s+", " ",regex=True)
    return data


'''text="i am happy today :)"
print(textpreprocessing(pd.DataFrame(
    {"text":[text]

    })))
'''



