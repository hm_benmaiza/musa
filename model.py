import tensorflow as tf
import tensorflow_text as text
import tensorflow_hub as hub
from tensorflow.keras.layers import BatchNormalization,Dense,Input,LSTM,Embedding,Conv2D,MaxPooling2D,GlobalAveragePooling2D,Concatenate,Dropout,Bidirectional,GlobalMaxPooling2D
from tensorflow.keras.models import Sequential,Model,load_model


shape=224

#function to build nlp model
def build_bert_model(input,h=128):
  preprocessing_layer = hub.KerasLayer("preprocess/", name='preprocessing')
  encoder = hub.KerasLayer("encoder/", trainable=True, name='BERT_encoder')

  encoder_inputs = preprocessing_layer(input)
  outputs = encoder(encoder_inputs)
  net = outputs['pooled_output'] #786
  net = tf.keras.layers.Dropout(0.3)(net)
  net = tf.keras.layers.Dense(h, activity_regularizer=tf.keras.regularizers.L1())(net)
  net = tf.keras.layers.Dropout(0.3)(net)
  return tf.keras.Model(input, net)




#function to build cv model
def cv_model(input_image,h=128):


    m=tf.keras.applications.InceptionResNetV2(include_top=False,input_shape=(shape,shape,3))

    x=m(input_image)
    #x=tf.keras.layers.Reshape((-1, 1280))(x)
    #x=attention()(x)
    x=tf.keras.layers.GlobalAveragePooling2D()(x)
    x=tf.keras.layers.Dropout(0.3)(x)
    x=tf.keras.layers.Dense(h, activity_regularizer=tf.keras.regularizers.L1())(x)
    
    x = tf.keras.layers.Dropout(0.3)(x)
    return tf.keras.Model(input_image, x)


#function to build the global model
def CreateModel():
	#define the inputs
	input_image=Input((224,224,3))
	input_text=Input((),dtype=tf.string, name='text')


	#build the unimodal models
	text_model=build_bert_model(input_text,128)
	image_model=cv_model(input_image,128)


	#concatenate their outputs
	concat=tf.keras.layers.concatenate([image_model.output,text_model.output])


	#fully connected networkd
	x=tf.keras.layers.Dense(128)(concat)
	x=tf.keras.layers.BatchNormalization()(x)
	x=tf.keras.layers.Activation("relu")(x)
	x=tf.keras.layers.Dropout(0.5)(x)

	x=tf.keras.layers.Dense(128)(concat)
	x=tf.keras.layers.Activation("relu")(x)
	x=tf.keras.layers.Dropout(0.5)(x)

	last=tf.keras.layers.Dense(3,activation="softmax" )(x)
	
	model=tf.keras.models.Model([input_image,input_text],last)
	model.compile(optimizer=tf.keras.optimizers.Adam(3e-5),metrics=["accuracy"],loss="sparse_categorical_crossentropy")
	model.load_weights("fusion.h5")

	return model
